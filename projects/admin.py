from django.contrib import admin
from .models import portfolio

# Register your models here.

@admin.register(portfolio)
class catadmin(admin.ModelAdmin):
    list_display = ['id','name','title','image','details']
