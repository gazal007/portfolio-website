from django.db import models

# Create your models here.


class portfolio(models.Model):
    name = models.CharField(max_length=50)
    title = models.CharField(max_length = 500)
    image = models.ImageField(upload_to='portfolio\pictures', max_length=255)
    details = models.TextField()
    Show = models.BooleanField(default = False, value = True)


    def __str__(self):
        return self.name

class BlogPost(models.Model):
	title 				= models.CharField(max_length=50, null=True, blank=False)
	body 				= models.TextField(max_length=5000, null=True, blank=False)
	image 				= models.ImageField(upload_to='portfolio\pictures', null=True, blank=False)
	date_published 		= models.DateTimeField(auto_now_add=True, verbose_name="date published")
	date_updated 		= models.DateTimeField(auto_now=True, verbose_name="date updated")
	#author 				= models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
	slug 				= models.SlugField(blank=True, unique=False)

	def __str__(self):
		return self.title